<?php

namespace App\Http\Controllers;
use App\Resto;
use App\InscriptionResto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
class Posts extends Controller
{
    function search(Request $request) {
        // AND inscription_restos.categories_cuisine LIKE "% ? %"
         $posts=DB::select('select inscription_restos.gouvernorat,inscription_restos.ville,restos.id,restos.name,restos.prix,restos.photo,adresse,inscription_restos.name as restoname  from restos JOIN inscription_restos ON restos.id_resto=inscription_restos.id WHERE inscription_restos.ville= ? AND inscription_restos.categories_cuisine = ? AND restos.prix BETWEEN ? AND ?',[$request->input('location'), $request->input('category'), $request->input('min'), $request->input('max')]);
         return view ('search',['posts'=>$posts]);
     }

   function allposts() {
        $posts=DB::select('select inscription_restos.gouvernorat,inscription_restos.ville,restos.id,restos.name,restos.prix,restos.photo,adresse,inscription_restos.name as restoname  from restos JOIN inscription_restos ON restos.id_resto=inscription_restos.id');
        return view ('allPosts',['posts'=>$posts]);
    }


    function post($id) {
        $post=DB::select('select restos.*, inscription_restos.numero_principal,inscription_restos.gouvernorat,inscription_restos.ville, inscription_restos.name as restoname,adresse,inscription_restos.latitude as lat, inscription_restos.longitude as longi from restos JOIN inscription_restos ON restos.id_resto=inscription_restos.id where restos.id=?', [$id]);
        foreach ($post as $po){
            $lat=$po->lat;
            $long=$po->longi;
        }
        return view ('post',['post'=>$post,"lat"=>$lat,"long"=>$long]);
   }
}