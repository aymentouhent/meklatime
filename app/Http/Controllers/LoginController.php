<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\InscriptionResto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;


class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('login');
    }

    public function indexInscri()
    {
        return view ('inscription');
    }

    public function indexInscriResto()
    {
        return view ('inscriptionResto');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function auth()
    // {
    //     $nom =  $request->input('email');
    //     $passe =  $request->input('passe');
    //     // $checkUser = DB::table('users')->select()->get() ;
    //     $checkUser = DB::table('users')->select()->where('email' ,$nom)->first() ;
	// 	if (!$checkUser) {
	// 		return view('connecter', ['invalidLogin'=>true]);
	// 	}
    //     if ($checkUser->email == $nom) {
    //         $getPassword = DB::table('users')->select('password')->where('email' , $nom)->first() ;
    //         // dd($getPassword);
    //         if (password_verify($passe,$getPassword->password)){
    //             $request->session()->put('username' ,$nom) ;
    //             $request->session()->put('userid' ,$checkUser->id) ;
    //             return redirect ('/detail');
    //         } else {
    //             return redirect ('/connecter');
                
    //         }
    //     }
    //     else {
    //         return redirect ('/connecter');
            
    //     }
    // }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/');
            $request->session()->put('useremail', $request->input('email'));
            $id = DB::table('users')->where('email',$request->input('email'))->value('id');
            $request->session()->put('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d', $id);
        } else {
			return view('login', ['invalidLogin'=>true]);
            // return redirect()->intended('connecter')->with(['invalidLogin'=>true]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function register(Request $request)
    {

        
        try {
            $user = new User();
            $user->password = Hash::make($request->input('password'));
            $user->email = $request->input('email');
            $user->name = $request->input('name');
            $user->role_id = 2;
            $user->save();
            return redirect('/connecter')->with('success', 'Nous vous remercions pour votre inscription, un email avec les détails de connexion vous est envoyé.');

            // $pass = Hash::make($request->input('password'));
            // DB::table('users')->insert([
            //     ['name' => $request->input('nom'), 'email' => $request->input('email'), 'password' => $pass, 'role_id' => '2'],
            // ]);
            // DB::table('inscriptions')->insert([
            //     ['nom_prenom' => $request->input('nom'), 'numero' => $request->input('numero'), 'email' => $request->input('email')],
            // ]);
            // DB::table('inscriptions')->insert([
            //     ['nom_prenom' => $request->input('nom'), 'numero' => $request->input('numero'), 'email' => $request->input('email')],
            // ]);
            // DB::table('inscriptions')->insert([
            //     ['nom_prenom' => $request->input('nom'), 'numero' => $request->input('numero'), 'email' => $request->input('email')],
            // ]);
            
            // $request->session()->put('username', $request->input('email'));
		
		    // $data = array('nom' => $request->input('nom'), 'email' => $request->input('email'), 'passe' => $request->input('passe'));
            
            // Mail::send('emails.contact', ['contact' => $data] , function ($message) use ($data) {
            //     //remitente
            //     $message->from('webinaire.stb@gmail.com', 'Webinaire STB');
            //     //asunto
            //     $message->subject('Merci pour votre inscription');
            //     //receptor
            //     $message->to($data['email']);
            // });
            
        
		} catch (\Exception $e) {
            return view('inscription', ['emailExists' => true]);
        }
    }




    public function registerResto(Request $request)
    {

        
        try {

             DB::table('users')->insert([
                [
                   "name" => $request->input('name'),"password" => Hash::make($request->input('password')),"email"=>$request->input('email'),"role_id"=>3
                ]
                ]);

            $id = DB::table('users')->where('email',$request->input('email'))->value('id');
            // dump($id);
            DB::table('inscription_restos')->insert([
                [
                'name' => $request->input('name'), 'numero_principal' => $request->input('numero_principal'), 'numero_secondaire' => $request->input('numero_secondaire'),
                'adresse' => $request->input('adresse'), 'gouvernorat' => $request->input('gouvernorat'), 'ville' => $request->input('ville'),
                'longitude' => $request->input('longitude'), 'latitude' => $request->input('latitude'), 'id_user' => $id
                ]
                ]);
            
            return redirect('/connecter')->with('success', 'Resto Nous vous remercions pour votre inscription, un email avec les détails de connexion vous est envoyé.');



            // $request->session()->put('username', $request->input('email'));
		
		    // $data = array('nom' => $request->input('nom'), 'email' => $request->input('email'), 'passe' => $request->input('passe'));
            
            // Mail::send('emails.contact', ['contact' => $data] , function ($message) use ($data) {
            //     //remitente
            //     $message->from('webinaire.stb@gmail.com', 'Webinaire STB');
            //     //asunto
            //     $message->subject('Merci pour votre inscription');
            //     //receptor
            //     $message->to($data['email']);
            // });
            
        
		} catch (\Exception $e) {
            return view('inscription', ['emailExists' => true]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->session()->flush();
        // $request->session()->forget();
        // dd($request->session());
        return redirect ('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}