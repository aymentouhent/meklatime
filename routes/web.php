<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $posts=DB::select('select inscription_restos.gouvernorat,inscription_restos.ville,restos.id,restos.name,restos.prix,restos.photo,adresse,inscription_restos.name as restoname  from restos JOIN inscription_restos ON restos.id_resto=inscription_restos.id LIMIT 3');
    $ville=DB::select('select ville from inscription_restos GROUP BY ville');
    $category=DB::select('select categories_cuisine from inscription_restos GROUP BY categories_cuisine');
    return view('home',['posts'=>$posts,'ville'=>$ville,'category'=>$category]);
});

// Route::group(['middleware' => 'checkuser'] , function() {
//     // Route::get('/inscription','LoginController@indexInscri');
// });

Route::put("/search", 'Posts@search');
Route::get('/connecter','LoginController@index')->middleware('guest');
Route::get('/inscription','LoginController@indexInscri')->middleware('guest');
Route::get('/inscription-resto','LoginController@indexInscriResto')->middleware('guest');
Route::put('/login', 'LoginController@authenticate');
Route::put('/register', 'LoginController@register');
Route::put('/register-resto', 'LoginController@registerResto');
Route::get('/logout', 'LoginController@logout');
Route::get('/posts', 'Posts@allposts');
Route::get("/posts/{id}", 'Posts@post');
// Route::get(function () {return view('404');});
// Route::put('/submit', 'InscriptionController@store');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});