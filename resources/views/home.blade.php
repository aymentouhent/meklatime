@extends('layouts.layout')

@section('content')

<div id="page-content">
    <div class="hero-section has-background height-450px">
        <div class="wrapper">
            <div class="inner">
                <div class="center">
                    <div class="page-title">
                        <h1>Best Deals in One Place</h1>
                        <h2>With Locations you can find the best deals in your location</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="background-wrapper">
            <div class="bg-transfer opacity-30"><img src="assets/img/background-03.jpg" alt=""></div>
            <div class="background-color background-color-black"></div>
        </div>
        <!--end background-wrapper-->
    </div>
    <section class="block background-is-dark">
        <div class="form search-form">
            <div class="container">
                <div class="section-title">
                    <h2 class="center">Find Deals</h2>
                </div>
                 <form action="{{URL::to('/search')}}" method="POST" name="search">
                        @csrf
                        @method('PUT')
                    <div class="row">
                       
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <select class="form-control selectpicker" name="location" required>
                                    <option value="">Location</option>
                                @foreach ($ville as $vil)
                                    <option value="{{$vil->ville}}">{{$vil->ville}}</option>
                                @endforeach
                                </select>
                            </div>
                      
                        </div>
                     
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <select class="form-control selectpicker" name="category" required>
                                <option value="">Category</option>
                                @foreach ($category as $categ)
                                    <option value="{{$categ->categories_cuisine}}">{{$categ->categories_cuisine}}</option>
                                @endforeach
                                </select>
                            </div>
                          
                        </div>
                    
                        <div class="col-md-3 col-sm-4">
                            <div class="form-group">
                          
                              <div class="ui-slider" id="price-slider" data-value-min="1" data-value-max="50" data-value-type="price" data-currency="" data-currency-placement="before">
                                    <div class="values clearfix">
                                        <input class="value-min" name="min" readonly>
                                        <input class="value-max" name="max" readonly>
                                    </div>
                                    <div class="element"></div>
                                </div>
                                </div>
                       
                        </div>
                      
                        <div class="col-md-1 col-sm-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary width-100 darker"><i
                                        class="fa fa-search"></i></button>
                            </div>
                          
                        </div>
                      
                    </div>
              
                </form> 
             
            </div>
          
        </div>
      
        <div class="background-wrapper">
            <div class="background-color background-color-default"></div>
            <div class="bg-transfer opacity-40"><img src="assets/img/background-04.jpg" alt=""></div>
        </div>
    </section>
    <section class="block">
        <div class="container">
            <div class="center">
                <div class="section-title">
                    <div class="center">
                        <h2>Recent Places</h2>
                        <h3 class="subtitle">Fusce eu mollis dui, varius convallis mauris. Nam dictum id</h3>
                    </div>
                </div>
                <!--end section-title-->
            </div>
            <!--end center-->
                 <div class="row">
                @foreach ($posts as $post)
                <div class="col-md-4 col-sm-4">
                    <div class="item" data-id="1">
                        <a href="{{ URL('posts/'.$post->id)}}">
                            <div class="description">
                                <figure>Average Price: {{$post->prix}} DT</figure>
                                <div class="label label-default">{{$post->name}}</div>
                                <h3>{{$post->restoname}} Restaurant</h3>
                                <h4>{{$post->adresse}} {{$post->gouvernorat}} {{$post->ville}}</h4>
                            </div>

                            <div class="image bg-transfer">
                                <img src="{{ url('storage/'.$post->photo)}}" alt="{{$post->name}}">
                            </div>

                        </a>
        

                    </div>
                </div>

                @endforeach
            </div>
            <!--end row-->
            <div class="center">
                <a href="/posts" class="btn btn-primary btn-light-frame btn-rounded btn-framed arrow">View all
                    listings</a>
            </div>
            <!--end center-->
        </div>
        <!--end container-->
    </section>
</div>
@endsection
