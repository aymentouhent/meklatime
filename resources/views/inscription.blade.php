@extends('layouts.layout')

@section('content')

<div id="page-content">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/inscription">Register</a></li>
        </ol>
        <!--end breadcrumb-->
        <div class="row">
            <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                <section class="page-title">
                    <h1>Register</h1>
                </section>
                <!-- end page-title-->
                <section>
                    <form class="form inputs-underline" name="login" action="{{URL::to('/register')}}" method="post">
                        @csrf
                        @method('PUT')
                        @if (isset($emailExists) && $emailExists)
                        <div class="col-12 d-flex">
                            <div class="mx-auto mb-2 alert-log">Adresse Email déjà exitante</div>
                        </div>
                        @endif


                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="name" required>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                        </div>
                        <!--end form-group-->
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password"
                                placeholder="Password" required>
                        </div>
                        <!--end form-group-->
                        <div class="form-group center">
                            <button type="submit" class="btn btn-primary width-100">Register Now</button>
                        </div>
                        <!--end form-group-->
                    </form>
                </section>
            </div>
            <!--col-md-4-->
        </div>
        <!--end ro-->
    </div>
    <!--end container-->
</div>
<!--end page-content-->

@endsection