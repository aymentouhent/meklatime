@extends('layouts.layout')
@section('content')

@if (\Session::has('success'))
<div class="alert alert-success customAlert" style="position: absolute;
    top: 10;
    width: 400px;
    right: 10;">
    <ul>
        <li>{!! \Session::get('success') !!}</li>
    </ul>
</div>
@endif

<div id="page-content">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/inscription">Register</a></li>
        </ol>
        <!--end breadcrumb-->
        <div class="row">
            <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                <section class="page-title">
                    <h1>Sign In</h1>
                </section>
                <!-- end page-title-->
                <section>
                    <form class="form inputs-underline" name="login" action="{{URL::to('/login')}}" method="post">
                        @csrf
                        @method('PUT')
                        @if (isset($invalidLogin) && $invalidLogin)
                        <div class="col-12 d-flex">
                            <div class="mx-auto mb-2 alert-log">Authentification invalide</div>
                        </div>
                        @endif

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                        </div>
                        <!--end form-group-->
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password"
                                placeholder="Password" required>
                        </div>
                        <!--end form-group-->
                        <div class="form-group center">
                            <button type="submit" class="btn btn-primary width-100">LogIn Now</button>
                        </div>
                        <!--end form-group-->
                    </form>
                </section>
            </div>
            <!--col-md-4-->
        </div>
        <!--end ro-->
    </div>
    <!--end container-->
</div>

@endsection