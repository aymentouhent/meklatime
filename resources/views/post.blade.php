@extends('layouts.layout')

@section('content')
@foreach ($post as $p)
<div id="page-content">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/posts">Posts</a></li>
            <li class="active" href="/posts/{{$p->id}}">Detail</li>
        </ol>
        <section class="page-title pull-left">
            <h1>{{$p->restoname}} Restaurant</h1>
            <h3>{{$p->adresse}} {{$p->gouvernorat}} {{$p->ville}}</h3>

        </section>

    </div>


    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-7">
                <img src="{{ url('storage/'.$p->photo)}}" style="max-width: 500px;">

                <section>
                    <h2>Ingidients</h2>
                    <ul class="tags">
                        @php
                            $str=explode(" ",$p->ingredient);
                            str_replace([",", "."]," ",$str);
                            foreach($str as $val){
                       
                        echo "<li>".$val."</li>";
                               
                            }
                        @endphp
                
                    </ul>
                </section>
                <section>
                    <h2>Opening Hours</h2>
                    <dl>
                        <dt>Monday</dt>
                        <dd>08:00am - 11:00pm</dd>
                        <dt>Tuesday</dt>
                        <dd>08:00am - 11:00pm</dd>
                        <dt>Wednesday</dt>
                        <dd>12:00am - 11:00pm</dd>
                        <dt>Thursday</dt>
                        <dd>08:00am - 11:00pm</dd>
                        <dt>Friday</dt>
                        <dd>03:00pm - 02:00am</dd>
                        <dt>Saturday</dt>
                        <dd>03:00pm - 02:00am</dd>
                        <dt>Sunday</dt>
                        <dd>Closed</dd>
                    </dl>
                </section>

            </div>
            <!--end col-md-7-->
            <div class="col-md-5 col-sm-5">
                <div class="detail-sidebar">
                    <section class="shadow">
                        <div class="map height-250px" id="map-detail"></div>
                        <!--end map-->
                        <div class="content">
                        
                            <hr>
                            <address>
                                <figure><i class="fa fa-map-marker"></i>{{$p->adresse}} <br> {{$p->gouvernorat}}
                                    {{$p->ville}}
                                </figure>
                                @if(Auth::check())
                                <figure><i class="fa fa-envelope"></i><a href="#">{{Auth::user()->email}}</a></figure>
                                <figure><i class="fa fa-phone"></i>{{$p->numero_principal}}</figure>
                                @else
                                <figure><a href="/connecter">You need to log in to other credentiels</a></figure>
                              
                                @endif
                            </address>
                        </div>
                    </section>

                </div>
                <!--end detail-sidebar-->
            </div>
            <!--end col-md-5-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</div>
@endforeach
@section('script')
<script>
rating(".visitor-rating");
var _latitude = <?= json_encode($lat) ?>;
var _longitude = <?= json_encode($long) ?>;
console.log(_latitude, _longitude)
var element = "map-detail";
simpleMap(_latitude, _longitude, element);
</script>
@endsection
@endsection