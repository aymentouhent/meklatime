@extends('layouts.layout')
@section('content')


<div id="page-content">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/inscription">Register Restorateur</a></li>
        </ol>
        <!--end breadcrumb-->
        <div class="row">
            <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                <section class="page-title">
                    <h1>Register</h1>
                </section>
                <!-- end page-title-->
                <section>
                    <form class="form inputs-underline" name="login" action="{{URL::to('/register-resto')}}"
                        method="post">
                        @csrf
                        @method('PUT')
                        @if (isset($emailExists) && $emailExists)
                        <div class="col-12 d-flex">
                            <div class="mx-auto mb-2 alert-log">Adresse Email déjà exitante</div>
                        </div>
                        @endif


                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="name" required>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                        </div>
                        <!--end form-group-->
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password"
                                placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <label for="numero_principal">Principal Number</label>
                            <input type="text" class="form-control" name="numero_principal" id="numero_principal"
                                placeholder="Principal Number" required>
                        </div>
                        <div class="form-group">
                            <label for="numero_secondaire">Secondaire Number</label>
                            <input type="text" class="form-control" name="numero_secondaire" id="numero_secondaire"
                                placeholder="Secondaire Number" required>
                        </div>
                        <div class="form-group">
                            <label for="adresse">Adresse</label>
                            <input type="text" class="form-control" name="adresse" id="adresse" placeholder="adresse" required>
                        </div>
                        <div class="form-group">
                            <label for="gouvernorat">Gouvernorat</label>
                            <input type="text" class="form-control" name="gouvernorat" id="gouvernorat"
                                placeholder="gouvernorat" required>
                        </div>
                        <div class="form-group">
                            <label for="ville">Ville</label>
                            <input type="text" class="form-control" name="ville" id="ville" placeholder="ville" required>
                        </div>
                        <div class="form-group">
                            <label for="longitude">Longitude Number</label>
                            <input type="text" class="form-control" name="longitude" id="longitude"
                                placeholder="longitude" required>
                        </div>
                        <div class="form-group">
                            <label for="latitude">Latitude Number</label>
                            <input type="text" class="form-control" name="latitude" id="latitude"
                                placeholder="latitude" required>
                        </div>

                        <!--end form-group-->
                        <div class="form-group center">
                            <button type="submit" class="btn btn-primary width-100">Register Now</button>
                        </div>
                        <!--end form-group-->
                    </form>
                </section>
            </div>
            <!--col-md-4-->
        </div>
        <!--end ro-->
    </div>
    <!--end container-->
</div>
<!--end page-content-->


@endsection