<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <base href="/public">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="assets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet'
        type='text/css'>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/zabuto_calendar.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">

    <link rel="stylesheet" href="assets/css/jquery.nouislider.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
</head>

<body>
    <div class="page-wrapper">
        <header id="page-header">
            <nav>
                <div class="left">
                    <a href="/" class="brand"><img src="assets/img/logo.png" alt=""></a>
                </div>
                <!--end left-->
                <div class="right">
                    <div class="primary-nav">

                        <a class="{{ request()->is('/') ? 'promoted' : '' }}" href="/">Home</a>

                        <!--end navigation-->
                    </div>
                    @if (!Auth::check())
                    <!--end primary-nav-->
                    <div class="secondary-nav">
                        <a class="{{ request()->is('inscription') ? 'promoted' : '' }}" href="/inscription">Register</a>
                        <a class="{{ request()->is('inscription-resto') ? 'promoted' : '' }}" href="/inscription-resto"
                            class="promoted">Register Restaurateur</a>
                    </div>
                    <!--end secondary-nav-->
                    <a href="/connecter"
                        class=" {{ request()->is('connecter') ? 'promoted' : '' }} btn btn-primary btn-small btn-rounded icon shadow add-listing"><i
                            class="fa fa-plus"></i><span>Sign In</span></a>

                    @else
                    @if (Auth::user()->role_id!=2)
                    <a href="/admin" class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><i
                            class="fa fa-plus"></i><span>Admin</span></a>
                    @endif
                    <a href="/logout"
                        class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><span>Logout</span></a>
                    @endif
                    <!--end nav-btn-->
                </div>
                <!--end right-->
            </nav>
            <!--end nav-->
        </header>

        @yield('content')

        <footer id="page-footer">
            <div class="footer-wrapper">
                <div class="block">
                    <div class="container">
                        <div class="vertical-aligned-elements">
                            <div class="element width-50">
                                <p data-toggle="modal" data-target="#myModal">Quisque aliquam at neque sit amet
                                    vestibulum. <a href="#">Terms of
                                        Use</a> and <a href="#">Privacy Policy</a>.</p>
                            </div>
                            <div class="element width-50 text-align-right">
                                <a href="#" class="circle-icon"><i class="social_twitter"></i></a>
                                <a href="#" class="circle-icon"><i class="social_facebook"></i></a>
                                <a href="#" class="circle-icon"><i class="social_youtube"></i></a>
                            </div>
                        </div>
                        <div class="background-wrapper">
                            <div class="bg-transfer opacity-50">
                                <img src="assets/img/footer-bg.png" alt="">
                            </div>
                        </div>
                        <!--end background-wrapper-->
                    </div>
                </div>
                <div class="footer-navigation">
                    <div class="container">
                        <div class="vertical-aligned-elements">
                            <div class="element width-50">(C) 2016 Your Company, All right reserved</div>
                            <div class="element width-50 text-align-right">
                                <a href="index.html">Home</a>
                                <a href="listing-grid-right-sidebar.html">Listings</a>
                                <a href="submit.html">Submit Item</a>
                                <a href="contact.html">Contact</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script type="text/javascript" src="assets/js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>
    <script type="text/javascript"
        src="http://maps.google.com/maps/api/js?key=AIzaSyC0pvD0gTjRww_yyrF6zNE2Bvz1GoMOZk0&libraries=places"></script>
    <script type="text/javascript" src="assets/js/richmarker-compiled.js"></script>
    <script type="text/javascript" src="assets/js/markerclusterer_packed.js"></script>
    <script type="text/javascript" src="assets/js/infobox.js"></script>
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="assets/js/moment.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/icheck.min.js"></script>
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.nouislider.all.min.js"></script>
    <script type="text/javascript" src="assets/js/custom.js"></script>
    <script type="text/javascript" src="assets/js/maps.js"></script>
    @yield('script')
</body>

</html>