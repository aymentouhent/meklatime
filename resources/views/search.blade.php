@extends('layouts.layout')
@section('content')

<div id="page-content">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active" href="/posts">Posts</li>
        </ol>

        <section class="page-title">
            <h1>All Posts</h1>
        </section>
        <!--end section-title-->
        <!-- 
        <section>
            <div class="search-results-controls clearfix">
                <div class="pull-left">
                    <a href="listing-grid-right-sidebar.html" class="circle-icon active"><i class="fa fa-th"></i></a>
                    <a href="listing-row-right-sidebar.html" class="circle-icon"><i class="fa fa-bars"></i></a>
                </div>
             
                <div class="pull-right">
                    <div class="input-group inputs-underline min-width-150px">
                        <select class="form-control selectpicker" name="sort">
                            <option value="">Sort by</option>
                            <option value="1">Price</option>
                            <option value="2">Distance</option>
                            <option value="3">Title</option>
                        </select>
                    </div>
                </div>
              
            </div>
          
        </section> -->

        <section>
            <div class="row">
                @foreach ($posts as $post)
                <div class="col-md-4 col-sm-4">
                    <div class="item" data-id="1">
                        <a href="{{ URL('posts/'.$post->id)}}">
                            <div class="description">
                                <figure>Average Price: {{$post->prix}} DT</figure>
                                <div class="label label-default">{{$post->name}}</div>
                                <h3>{{$post->restoname}} Restaurant</h3>
                                <h4>{{$post->adresse}} {{$post->gouvernorat}} {{$post->ville}}</h4>
                            </div>

                            <div class="image bg-transfer">
                                <img src="{{ url('storage/'.$post->photo)}}" alt="{{$post->name}}">
                            </div>

                        </a>
                  

                    </div>
                </div>

                @endforeach
                @if(!$posts)
                    <h2>No post was found</h2>
                @endif
            </div>
        </section>

        <!-- <section>
            <div class="center">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="disabled previous">
                            <a href="#" aria-label="Previous">
                                <i class="arrow_left"></i>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li class="active"><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li class="next">
                            <a href="#" aria-label="Next">
                                <i class="arrow_right"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </section> -->
    </div>
    <!--end container-->
</div>

@endsection